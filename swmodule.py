import socket
from random import randint
from checksum import *

def recvfrom(correctLen, host, port, timeout):
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    mySocket.settimeout(timeout)
    
    mySocket.bind((host, port))
    currentACK = 0
    totalData = []

    while correctLen > 0:
        try:
            response = mySocket.recvfrom(2 + 1000)
            recievedACK, recievedData = response[0][1], response[0][2:].decode()

            if randint(1, 30) > 10:
                if recievedACK == currentACK:
                    if not check(response[0]):
                        print("incorrect checksum")
                        continue
                    if currentACK == 1:
                        currentACK = 0
                    else:
                        currentACK = 1
                    correctLen -= len(recievedData)
                    totalData.append(recievedData)

                    print(f"Package {recievedACK} received. Package size is {len(recievedData)} byte.")

                mySocket.sendto(bytes([count(bytes([recievedACK]) + b"ACK")]) + bytes([recievedACK]) + b"ACK", response[1])
                print(f"ACK {recievedACK} sent.")
            else:
                raise socket.timeout()
        except socket.timeout:
            print("Reached timed out. Retrying...")
            continue

    return ''.join(totalData)

def sendto(data, host, port, timeout):
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    mySocket.settimeout(timeout)

    currentACK = 1 
    for package in [data[i:i + 1000] for i in range(0, len(data), 1000)]:
        if currentACK == 1:
            currentACK = 0
        else:
            currentACK = 1

        package = bytes([count(bytes([currentACK]) + package.encode(encoding="utf-8"))]) + bytes([currentACK]) + package.encode(encoding="utf-8")
        while True:
            mySocket.sendto(package, (host, port))
            try:
                print(f"Package {currentACK} sent.")
                if randint(1, 30) > 10:
                    if mySocket.recvfrom(2 + 3)[0] == bytes([count(bytes([currentACK]) + b"ACK")]) + bytes([currentACK]) + b"ACK":
                        print(f"ACK {currentACK} received.")
                        break
                else:
                    raise socket.timeout() 
            except socket.timeout:
                print("Reached timed out. Retrying...")
                continue