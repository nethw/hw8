def check(data, mod = 8):
    res = 0
    for i in range(0, len(data), mod // 8):
        res = (res + int.from_bytes(data[i:i + mod // 8], 'big')) % 2 ** mod
    return res == 2 ** mod - 1
    
def count(data, mod = 8):
    res = 0
    for i in range(0, len(data), mod // 8):
        res = (res + int.from_bytes(data[i:i + mod // 8], 'big')) % 2 ** mod
    return 2 ** mod - 1 - res

localTestString = b'Hello world!'

def test_success():
    assert check(bytes([count(localTestString)]) + localTestString)
    print("test success passed")

def test_failure():
    assert not check(bytes([count(localTestString) + 1]) + localTestString)
    print("test failure passed")