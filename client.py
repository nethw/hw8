import sys
import os
conf_path = os.getcwd()
sys.path.append(conf_path)
sys.path.append(conf_path + '\scripts\Setup') 

from swmodule import *

mode = input("Press:\n g - generate file\n s - send file\n r - recieve file\n")
reqData = ''.join([str(1) for i in range(3000)])

if mode == "g":
    file = open("clientSend.txt", 'wt', encoding='utf-8')
    file.write(reqData)

if mode == "s":
    file = open("clientSend.txt", 'rt', encoding='utf-8')
    reqData = file.read()
    sendto(reqData, '127.0.0.1', 12345, 1)
    print('Client data has been sent.')

if mode == "r":
    received_server_data = recvfrom(len(reqData), '127.0.0.1', 12345, 1)
    print(f'Server data has been received: {len(received_server_data)} byte(s).') 
    file = open("clientRecieve.txt", 'wt', encoding='utf-8')
    file.write(received_server_data)

