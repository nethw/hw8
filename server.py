import sys
import os
conf_path = os.getcwd()
sys.path.append(conf_path)
sys.path.append(conf_path + '\scripts\Setup') 

from swmodule import *
from checksum import *

mode = input("Press:\n g - generate file\n t - test check sum\n s - send file\n r - recieve file\n")
reqData = ''.join([str(2) for i in range(3000)])

if mode == "g":
    file = open("serverSend.txt", 'wt', encoding='utf-8')
    file.write(reqData)

if mode == "s":
    file = open("serverSend.txt", 'rt', encoding='utf-8')
    reqData = file.read()
    sendto(reqData, '127.0.0.1', 12345, 1)
    print('Server data has been sent.')

if mode == "r":
    received_client_data = recvfrom(len(reqData), '127.0.0.1', 12345, 1)
    print(f'Client data has been received: {len(received_client_data)} byte(s).')
    file = open("serverRecieve.txt", 'wt', encoding='utf-8')
    file.write(received_client_data)

if mode == "t":
    test_success()
    test_failure()